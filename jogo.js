
var jogador = 0;
var jogadas = 0;

function clique(id) {

	click();

	console.log("jogador: "+jogador);
	
	var tag = null;
	
	if(id.currentTarget)
		tag = id.currentTarget; 
	else
		tag = document.getElementById('casa'+id);

	if(tag.style.backgroundImage == '' || tag.style.backgroundImage == null)
	{
		
		var endereco = 'img/'+jogador+'.png';
		tag.style.backgroundImage = 'url('+endereco+')';
		jogadas += 1;

		if (jogadas >=5 ){
			var ganhou = verificarGanhador();
		}
		
		switch(ganhou)
		{
			case 1:
				if(jogador == 0)
				{
					finalizar(0);
					var perdedor = 1
				}
				else
				{
					finalizar(1);
					var perdedor = 0
				}
				break;

			case -1:
				{
					finalizar(2);
				}
				break;
			case 0:
				// ninguem ganhou ainda
				break;
		}

		vez = document.getElementById('vez');
		if(jogador == 0)
		{
			aplicarefeitomihawk(true);
			aplicarefeitoshanks(false);
			jogador = 1;
			vez.innerHTML = 'Mihawk';
		}else
		{
			aplicarefeitoshanks(true);
			aplicarefeitomihawk(false);
			jogador = 0;
			vez.innerHTML = 'Shanks';
		}
	}
}

function verificarGanhador(){
	// pegar elementos
	c1 = document.getElementById('casa1');
	c1 = c1.style.backgroundImage;
	console.log(c1);
	c2 = document.getElementById('casa2');
	c2 = c2.style.backgroundImage;
	c3 = document.getElementById('casa3');
	c3 = c3.style.backgroundImage;
	c4 = document.getElementById('casa4');
	c4 = c4.style.backgroundImage;
	c5 = document.getElementById('casa5');
	c5 = c5.style.backgroundImage;
	c6 = document.getElementById('casa6');
	c6 = c6.style.backgroundImage;
	c7 = document.getElementById('casa7');
	c7 = c7.style.backgroundImage;
	c8 = document.getElementById('casa8');
	c8 = c8.style.backgroundImage;
	c9 = document.getElementById('casa9');
	c9 = c9.style.backgroundImage;
	
	if( (c1==c2 && c2==c3 && c1!='') ||
		(c4==c5 && c5==c6 && c4!='') ||
		(c7==c8 && c8==c9 && c7!='') ||
		(c1==c4 && c4==c7 && c1!='') ||
		(c2==c5 && c5==c8 && c2!='') ||
		(c3==c6 && c6==c9 && c3!='') ||
		(c1==c5 && c5==c9 && c1!='') ||
		(c3==c5 && c5==c7 && c3!=''))
	{
		return 1; // ganhou

	}else if(jogadas == 9){
		return -1;
	}
	return 0;
}

function finalizar(player){
	if (player == 0)
	{
		var vencedor = document.getElementById('vencedor');
		vencedor.innerHTML = '<h2>SHANKS VENCEU!</h2><button onclick = "iniciar(0)" type="button" class="btn btn-success m-3">Jogar de Novo!</button>';
		var vencedor = document.getElementById('vencedor');
		vencedor.style.backgroundImage = "url('img/shanks.gif')";
		vencedor.style.height="70%";
		console.log(vencedor);
	}
	if (player == 1)
	{
		var vencedor = document.getElementById('vencedor');
		vencedor.innerHTML = '<h2>MIHAWK VENCEU!</h2><button onclick = "iniciar(1)" type="button" class="btn btn-success m-3">Jogar de Novo!</button>';
		var vencedor = document.getElementById('vencedor');
		vencedor.style.backgroundImage = "url('img/mihawk.gif')";
		vencedor.style.height="70%";
		console.log(vencedor);
	}
	if (player == 2)
	{
		var vencedor = document.getElementById('vencedor');
		vencedor.innerHTML = '<h2>EMPATE!</h2 class="mt-4"><button onclick = "iniciar(2)" type="button" class="btn btn-success m-3">Jogar de Novo!</button';
		var vencedor = document.getElementById('vencedor');
		vencedor.style.backgroundImage = "url('img/empate.gif')";
		vencedor.style.height="70%";
		console.log(vencedor);
	}

	//start = confirm('Deseja iniciar nova partida?');
	// if(start)
	//	iniciar();
	tags = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		tags[i].onclick = null;
	}
}

function iniciar(jogador) {
	click();
	var vencedor = document.getElementById('vencedor');
	vencedor.style.height="0%";
	vencedor.innerHTML = '';
	if (jogador == 0){
		aplicarefeitomihawk(true);
		aplicarefeitoshanks(false);
		vez = document.getElementById('vez');
		vez.innerHTML = 'Mihawk';
		jogador = 1;
	}else if(jogador == 1) {
		aplicarefeitoshanks(true);
		aplicarefeitomihawk(false);
		vez = document.getElementById('vez');
		vez.innerHTML = 'Shanks';
		jogador = 0;
	} else {
		jogador = 0;
	}
	jogadas = 0;

	casas = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		casas[i].onclick=clique;
		casas[i].style.backgroundImage='';
	}

}

function musica(resposta){
	click();
	console.log(resposta);
	if (resposta){
		var musica = document.getElementById('musicafundo');
		musica.play();
		var botao = document.getElementById('audiobuttonhabilitar');
		botao.id = 'audiobuttondesabilitar';
		botao.setAttribute('onclick','musica(false)');
		botao.innerHTML = '<i id="icone" class="fas fa-volume-mute p-1"></i>Desabilitar música de fundo'
		// icone.className = 'fas fa-volume-mute p-1';
	}
	else{
		var musica = document.getElementById('musicafundo');
		console.log(musica);
		musica.pause();
		musica.currentTime = 0;
		var botao = document.getElementById('audiobuttondesabilitar');
		botao.id = 'audiobuttonhabilitar';
		botao.setAttribute('onclick','musica(true)');
		botao.innerHTML = '<i id="icone" class="fas fa-volume-up p-1"></i>Habilitar música de fundo';
	}
}

function aplicarefeitomihawk(resp){
	if (resp){
		var imagem = document.getElementById('lateralmihawk');
		imagem.style.filter = "grayscale(0%)";
		imagem.style.width="300px";
		imagem.style.height="300px";
	} else{
		var imagem = document.getElementById('lateralmihawk');
		imagem.style.filter = "grayscale(100%)";
		imagem.style.width="200px";
		imagem.style.height="200px";
	}
}

function aplicarefeitoshanks(resp){
	if (resp){
		var imagem = document.getElementById('lateralshanks');
		imagem.style.filter = "grayscale(0%)";
		imagem.style.width="300px";
		imagem.style.height="300px";
	} else{
		var imagem = document.getElementById('lateralshanks');
		imagem.style.filter = "grayscale(100%)";
		imagem.style.width="200px";
		imagem.style.height="200px";
	}
}

function click(){
	var clique = document.getElementById('cliquesound');
	clique.play();
}
// 1- verificar ganhador OK
// 2- exibir resultado OK
// 3- Mensagem quando der velha. OK
// 4- Indicar o jogador da vez - OK
// 5- Iniciar nova partida - OK
// 6- Finalização do jogo - OK
// 7- Placar (usando localStorage) https://www.w3schools.com/jsref/prop_win_localstorage.asp

/**
Itens do Trabalho (individual)
1- Melhorar quando se começa a verificar ganhador (a partir da 5 jogada)
2- Melhorar a verificação das linhas para saber se alguém ganhou...
 reduzir de 8 verificações para o mínimo.
4- Trocar alert por mensagem de texto no html.
5- Definir personagens  antagonistas e visual harmonico para o tabuleiro (css)
*/
